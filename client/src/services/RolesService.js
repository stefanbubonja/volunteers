import Api from '@/services/Api'

export default {
  getRoles () {
    return Api().get('/roles')
  }
}
