// TODO: prosiriti da prima rolu kao parametar, da ne bude zakucano ovako
// var permit = (req, res, next) => {
//   if (req.user && req.user.role.role_name === 'SUPERADMIN') {
//     next() // role is allowed, so continue on the next middleware
//   } else {
//     res.status(403).send()// user is forbidden
//   }
// }

// module.exports = {permit}

module.exports = function permit (...allowed) {
  const isAllowed = role => allowed.indexOf(role) > -1

  return (request, response, next) => {
    if (request.user && isAllowed(request.user.role.role_name)) {
      next()
    } else {
      response.status(403).json()
    }
  }
}
