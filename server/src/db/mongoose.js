const { mongodbUri } = require('../../config/config')
const mongoose = require('mongoose')
mongoose.Promise = global.Promise
// mongoose.connect(encodeURI(mongodbUri), {useNewUrlParser: true})
mongoose.connect(encodeURI(mongodbUri), {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true}).then(() => {
    console.log('Mongo connection started')
  })
  .catch((err) => {
    console.log('Error on start: ' + err.stack)
    process.exit(1)
})
module.exports = {mongoose}
