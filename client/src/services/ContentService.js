import Api from '@/services/Api'

export default {
  getCiteis () {
    return Api().get('/api/get-cities')
  },
  getContent () {
    return Api().get('/api/get-content')
  },
  getHelps () {
    return Api().get('/api/get-helps')
  },
  saveVolonter (data) {
    return Api().post('/volonters', data)
  },
  editVolonter (data) {
    return Api().put('/volonters', data)
  },
  saveNeeders (data) {
    return Api().post('/needers', data)
  }
}
