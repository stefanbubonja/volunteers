import Api from '@/services/Api'

export default {
  getNeeders (pagination) {
    return Api().get(`/needers`, {
      params: {
        page: pagination.page,
        perPage: pagination.rowsPerPage,
        search: pagination.search,
        status: pagination.status
      }
    })
  },
  getVolontersFiltered (needer) {
    let formatedDate = {
      city: needer.city,
      helpType: needer.helpType
    }
    return Api().post('/volonters/filtered', formatedDate)
  },
  updateNeeder (volonter, needer) {
    let formatedDate = {
      volonter: volonter,
      needer: needer
    }
    return Api().put('/needers', formatedDate)
  },
  updateNeederStatus (id, status) {
    let formatedDate = {
      id: id,
      status: status
    }
    return Api().put('/needers/status', formatedDate)
  }
}
