const { port, cliUri, exposedHeaders, bodyParserLimit } = require('../config/config')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
// eslint-disable-next-line
const {mongoose} = require('./db/mongoose')

// Routes
const userRoutes = require('./routes/users')
const roleRoutes = require('./routes/roles')
const api = require('./routes/api')
const volonters = require('./routes/volonters.js')
const needers = require('./routes/needers.js')

const app = express()

app.use(morgan('combined'))
app.use(bodyParser.json({ limit: bodyParserLimit }))

const corsOptions = {
  origin: function (origin, callback) {
    if (cliUri.split(',').indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  },
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  exposedHeaders: exposedHeaders.split(',')
}
app.use(cors(corsOptions))

app.use('/users', userRoutes)
app.use('/roles', roleRoutes)
app.use('/api', api)
app.use('/volonters', volonters)
app.use('/needers', needers)

app.listen(port || 9000, () => {
  console.log(`Volonter Admin started up at port ${port}`)
})
