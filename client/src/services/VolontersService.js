import Api from '@/services/Api'

export default {
  getVolonters (pagination) {
    return Api().get(`/volonters`, {
      params: {
        page: pagination.page,
        perPage: pagination.rowsPerPage,
        sortby: pagination.sortBy,
        order: pagination.descending ? 1 : -1,
        search: pagination.search
      }
    })
  },
  addVolonter (Volonter) {
    return Api().post('/volonters', Volonter)
  },
  updateVolonter (Volonter) {
    return Api().put('/volonters', Volonter)
  },
  deleteVolonter (id) {
    return Api().delete(`/volonters/${id}`)
  }
}
