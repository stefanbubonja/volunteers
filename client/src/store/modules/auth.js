import auth from '../../services/AuthService'
import Api from '@/services/Api'

const state = {
  user: localStorage.getItem('user_data') ? JSON.parse(localStorage.getItem('user_data')) : null,
  userFront: localStorage.getItem('user_front') ? JSON.parse(localStorage.getItem('user_front')) : null
}

const getters = {
  isLoggedIn: (state) => {
    return state.user !== null
  },
  isLoggedInFront: (state) => {
    return state.userFront !== null
  },
  frontUser: (state) => {
    return state.userFront
  },
  isSuperAdmin: (state) => {
    if (state.user !== null && state.user.role.role_id === 1) {
      return true
    } else {
      return false
    }
  },
  getRole: (state) => {
    return state.user !== null && state.user.role.role_name
  },
  getMarketCoverage: (state) => {
    if (state.user !== null && state.user.role && state.user.role.marketCoverage) {
      return state.user.role.marketCoverage
    }
    return []
  }
}

const actions = {
  login ({commit, dispatch}, data) {
    dispatch('common/setLoading', true, {root: true})
    return auth.login(data).then(res => {
      // Add authorization header
      localStorage.setItem('authorization_token', res.headers['x-auth'])
      localStorage.setItem('user_data', JSON.stringify(res.data))
      Api().defaults.headers.common['x-auth'] = res.headers['x-auth']
      commit('setUser', res.data)
      dispatch('common/setLoading', false, {root: true})
      return Promise.resolve(res)
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  },
  logout ({dispatch, commit}) {
    dispatch('common/setLoading', true, {root: true})
    return auth.logout().then(res => {
      // clear authorization header
      localStorage.removeItem('authorization_token')
      localStorage.removeItem('user_data')
      commit('setUser', null)
      Api().defaults.headers.common['x-auth'] = ''
      dispatch('common/setLoading', false, {root: true})
      return Promise.resolve()
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  },
  logoutLocal ({commit}) {
    // clear authorization header
    localStorage.removeItem('authorization_token')
    localStorage.removeItem('user_data')
    commit('setUser', null)
    Api().defaults.headers.common['x-auth'] = ''
    Api().defaults.headers.common['x-auth-front'] = ''
    localStorage.removeItem('authorization_token_front')
    localStorage.removeItem('user_front')
    commit('setUserFront', null)
  },
  loginFront ({commit, dispatch}, data) {
    dispatch('common/setLoading', true, {root: true})
    return auth.loginFront(data).then(res => {
      // Add authorization header
      if (!res.data.error) {
        localStorage.setItem('authorization_token_front', res.headers['x-auth-front'])
        localStorage.setItem('user_front', JSON.stringify(res.data))
        Api().defaults.headers.common['x-auth-front'] = res.headers['x-auth-front']
        commit('setUserFront', res.data)
      }
      dispatch('common/setLoading', false, {root: true})
      return Promise.resolve(res)
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  },
  logoutFront ({dispatch, commit}) {
    dispatch('common/setLoading', true, {root: true})
    return auth.logoutFront().then(res => {
      // clear authorization header
      localStorage.removeItem('authorization_token_front')
      localStorage.removeItem('user_front')
      commit('setUserFront', null)
      Api().defaults.headers.common['x-auth-front'] = ''
      dispatch('common/setLoading', false, {root: true})
      return Promise.resolve()
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  },
  updateUserFront ({commit}, user) {
    localStorage.setItem('user_front', JSON.stringify(user))
    commit('setUserFront', user)
  }
}

const mutations = {
  setUser (state, user) {
    state.user = user
  },
  setUserFront (state, user) {
    state.userFront = user
  },
  setLoading (state, loading) {
    state.loading = loading
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
