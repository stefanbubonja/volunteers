const dotenv = require('dotenv')
const result = dotenv.config({path: `${__dirname}/../.env`})
if (result.error) {
  throw result.error
}
module.exports = {
  appName: process.env.APP_NAME,
  port: process.env.PORT,
  database: process.env.DB,
  mongodbUri: process.env.MONGODB_URI,
  paginationLimit: process.env.PAGINATION_LIMIT,
  cliUri: process.env.CLI_URL,
  exposedHeaders: process.env.EXPOSED_HEADERS,
  bodyParserLimit: process.env.BODY_PARSER_LIMIT,
  jwtSecret: process.env.JWT_SECRET,
  jwtExpire: process.env.JWT_EXPIRE
}
