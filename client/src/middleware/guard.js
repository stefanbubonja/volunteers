import store from '../store'
export default {
  guest (to, from, next) {
    next(!store.getters['auth/isLoggedIn'] ? true : {
      path: '/admin'
    })
  },
  guestFront (to, from, next) {
    next(!store.getters['auth/isLoggedInFront'] ? true : {
      path: '/dostupnost'
    })
  },
  auth (to, from, next) {
    next(store.getters['auth/isLoggedIn'] ? true : {
      path: '/admin-login',
      query: {
        redirect: to.fullPath
      }
    })
  },
  authFront (to, from, next) {
    next(store.getters['auth/isLoggedInFront'] ? true : {
      path: '/forbidden'
    })
  },
  authAdmin (to, from, next) {
    next(store.getters['auth/isSuperAdmin'] ? true : {
      path: '/forbidden'
    })
  },
  checkRole (to, from, next) {
    let checkUserRole = to.meta.allowedRoles.indexOf(store.getters['auth/getRole']) !== -1
    next(checkUserRole ? true : {
      path: '/forbidden'
    })
  }
}
