module.exports = {
  apps: [
    {
      name       : "VOLONTER_CLUSTER",
      script     : "./src/app.js",
      instances  : 0,
      exec_mode  : "cluster",
      max_memory_restart: '1G',
      autorestart: true,
      watch: false,
    }
  ]
};