import Vue from 'vue'
import Router from 'vue-router'
import Guard from '../middleware/guard'

/**
 * ADMIN ROUTES
 */
import HomeAdmin from '@/components/Admin/HomeAdmin'
import Login from '@/components/Admin/Login/Login'
import Dashboard from '@/components/Admin/Dashboard/Dashboard'
import Users from '@/components/Admin/Users/Users'
import Volonters from '@/components/Admin/Volonters/Volonters'

/**
 * FRONT ROUTES
 */
import HomeFront from '@/components/Front/HomeFront'
import Content from '@/components/Front/Content'
import HelperVolonter from '@/components/Front/HelperVolonter'
import Needers from '@/components/Front/Needers'
import Availability from '@/components/Front/Availability'

import NotFound404 from '@/components/404'
import Forbidden403 from '@/components/403'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/admin',
      component: HomeAdmin,
      children: [
        {
          path: '/',
          name: 'dashboard',
          component: Dashboard,
          beforeEnter: Guard.auth
        },
        {
          path: '/users',
          name: 'users',
          component: Users,
          beforeEnter: Guard.checkRole,
          meta: {allowedRoles: ['SUPERADMIN']}
        },
        {
          path: '/volonters',
          name: 'volonters',
          component: Volonters,
          beforeEnter: Guard.checkRole,
          meta: {allowedRoles: ['SUPERADMIN', 'OPERATER']}
        }
      ]
    },
    {
      path: '/admin-login',
      name: 'adminLogin',
      component: Login,
      beforeEnter: Guard.guest
    },
    {
      path: '/',
      component: HomeFront,
      children: [
        {
          path: '/',
          name: 'content',
          component: Content,
          beforeEnter: Guard.guestFront
        },
        {
          path: '/pomazem',
          name: 'helper',
          component: HelperVolonter,
          beforeEnter: Guard.guestFront
        },
        {
          path: '/pomoc',
          name: 'needers',
          component: Needers,
          beforeEnter: Guard.guestFront
        },
        {
          path: '/dostupnost',
          name: 'availability',
          component: Availability,
          beforeEnter: Guard.authFront
        }
      ]
    },
    {
      path: '/forbidden',
      name: 'forbidden',
      component: Forbidden403
    },
    {
      path: '*',
      name: '404',
      component: NotFound404
    }
  ]
})
