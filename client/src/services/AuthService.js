import Api from '@/services/Api'

export default {
  getUserData () {
    return Api().get('/users/me')
  },
  login (data) {
    return Api().post('/users/login', data)
  },
  logout () {
    return Api().delete('/users/me/token')
  },
  loginFront (data) {
    return Api().post('/volonters/login', data)
  },
  logoutFront () {
    return Api().delete('/volonters/me/token')
  }
}
