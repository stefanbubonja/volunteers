var {User} = require('../models/User')

const authenticate = async (req, res, next) => {
  let token = req.header('x-auth')

  User.findByToken(token).then((user) => {
    if (!user) {
      // eslint-disable-next-line
      return Promise.reject()
    }
    req.user = user
    req.token = token
    next()
  }).catch((err) => {
    res.status(401).send(err)
  })
}

module.exports = {authenticate}
