const state = {
  drawer: localStorage.getItem('drawer') ? JSON.parse(localStorage.getItem('drawer')) : null,
  lang: localStorage.getItem('lang') ? localStorage.getItem('lang') : 'en',
  loading: false,
  message: '',
  languages: {}
}

const getters = {
  loading (state) {
    return state.loading
  },
  drawer (state) {
    return state.drawer
  },
  languages (state) {
    return state.languages
  }
}

const mutations = {
  setLoading (state, loading) {
    state.loading = loading
  },
  setMessage (state, message) {
    state.message = message
  },
  setDrawer (state, val) {
    state.drawer = val
    localStorage.setItem('drawer', val)
  },
  setLanguage (state, lang) {
    state.lang = lang
  }
}

const actions = {
  setLoading ({commit}, loading) {
    commit('setLoading', loading)
  },
  setMessage ({commit}, message) {
    commit('setMessage', message)
  },
  setDrawer ({commit}, val) {
    commit('setDrawer', val)
  },
  changeLang ({commit}, lang) {
    commit('setLanguage', lang)
    localStorage.setItem('lang', lang)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
