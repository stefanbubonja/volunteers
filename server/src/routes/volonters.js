const express = require('express')
const router = express.Router()
const _ = require('lodash')

const {Volonters} = require('../models/Volonters')
const {authenticate} = require('../middleware/authenticate')
const {frontauth} = require('../middleware/frontauth')

// POST /volonters
router.post('/', (req, res) => {
  const body = _.pick(req.body, [
    'email',
    'password',
    'name',
    'surname',
    'phone',
    'city',
    'availability',
    'helpType'
  ])
  const volonters = new Volonters(body)

  volonters.save().then(volonter => {
    res.send(volonter)
  }).catch((e) => {
    res.status(400).send(e)
  })
})

// POST /volonters/login
router.post('/login', (req, res) => {
  var body = _.pick(req.body, ['email', 'password'])

  Volonters.findByCredentials(body.email, body.password).then((volonter) => {
    return volonter.generateAuthToken().then(token => {
      res.setHeader('x-auth-front', token)
      res.send(volonter)
    })
  }).catch((e) => {
    res.send({error: true, status: 'WRONG_CREDENTIALS'})
  })
})

// DELETE /volonters/me/token
router.delete('/me/token', frontauth, (req, res) => {
  req.volonter.removeToken(req.tokenfront).then(() => {
    res.status(200).send()
  }, () => {
    res.status(400).send()
  })
})

// PUT /volonters
router.put('/', frontauth, (req, res) => {
  const body = _.pick(req.body, [
    '_id',
    'availability',
    'helpType'
  ])

  Volonters.findById(req.body._id, function (err, volonter) {
    if (volonter) {
      volonter.availability = body.availability
      volonter.helpType = body.helpType

      volonter.save().then((volonter) => {
        res.status(200).send(volonter)
      }).catch((err) => {
        res.status(400).send(err)
      })
    } else {
      res.status(400).send(err)
    }
  })
})

// GET /volonters
router.get('/', authenticate, (req, res) => {
  var pagObj = {
    page: parseInt(req.query.page),
    limit: parseInt(req.query.perPage) ? parseInt(req.query.perPage) : 10
  }
  var queryObj1 = {'city.region.shortName': req.query.region}

  if (req.query.sortby) {
    var sort = {}
    sort[req.query.sortby] = req.query.order
    pagObj.sort = sort
  }

  if (req.query.search && req.query.search !== '') {
    var queryObj2 = {name: {$regex: '.*' + req.query.search + '.*', $options: 'i'}}
  }
  var queryObj = {...queryObj1, ...queryObj2}

  Volonters.paginate(queryObj, pagObj).then(response => {
    res.send(response)
  }).catch((e) => {
    console.log(e)
  })
})

// POST /volonters/filtered
router.post('/filtered', authenticate, (req, res) => {
  if (req.body.city && req.body.helpType) {
    var queryObj = {
      'city': req.body.city,
      'helpType.name': { $in: req.body.helpType.map(help => help.name) },
      'availability': true
    }
    Volonters.find(queryObj).then(response => {
      res.send(response)
    }).catch((e) => {
      console.log(e)
    })
  } else {
    res.send({error: true, message: 'Filter params not provided'})
  }
})

// DELETE /volonters/:id
router.delete('/:id', authenticate, (req, res) => {
  Volonters.findByIdAndRemove(req.params.id, (err, doc) => {
    if (doc !== null) {
      res.status(200).send(doc)
    } else {
      res.status(400).send(err)
    }
  })
})

// TODO: Ruta da vraca volontere za autocomplete po gradu

module.exports = router
