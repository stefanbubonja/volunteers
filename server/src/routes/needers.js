const express = require('express')
const router = express.Router()
const _ = require('lodash')

const {Needers} = require('../models/Needers')
const {authenticate} = require('../middleware/authenticate')

// POST /needers
router.post('/', (req, res) => {
  const body = _.pick(req.body, [
    'name',
    'surname',
    'phone',
    'city',
    'age',
    'address',
    'helpType',
    'helpText'
  ])
  const needers = new Needers(body)
  needers.status = "OPEN"
  needers.ip = req.connection.remoteAddress
  needers.created = new Date()
  needers.save().then(needer => {
    res.send(needer)
  }).catch((e) => {
    res.status(400).send(e)
  })
})

router.put('/', authenticate, (req, res) => {
  Needers.findOne({_id: req.body.needer._id}).then(needer => {
    if (needer.status !== 'PENDING') {
      needer.status = 'PENDING'
      needer.volonter = req.body.volonter
      needer.save().then(needer => {
        res.send(needer)
      }).catch((e) => {
        res.status(400).send(e)
      })
    } else {
      res.send({error: true, message: 'WRONG_VERSION'})
    }
  }).catch((e) => {
    res.status(400).send(e)
  })
})

router.put('/status', authenticate, (req, res) => {
  Needers.findOne({_id: req.body.id}).then(needer => {
    if (needer.status !== req.body.status) {
      needer.status = req.body.status
      needer.save().then(needer => {
        res.send(needer)
      }).catch((e) => {
        res.status(400).send(e)
      })
    } else {
      res.send({error: true, message: 'WRONG_VERSION'})
    }
  }).catch((e) => {
    res.status(400).send(e)
  })
})

// GET /needers
router.get('/', authenticate, (req, res) => {
  let pagObj = {
    page: parseInt(req.query.page),
    limit: parseInt(req.query.perPage) ? parseInt(req.query.perPage) : 10,
    sort: {created: -1}
  }

  let queryObj = {}
  if (req.query.status && req.query.status !== '') {
    queryObj['status'] = req.query.status
  }

  if (req.query.search && req.query.search !== '') {
    queryObj['address'] = {$regex: '.*' + req.query.search + '.*', $options: 'i'}
  }

  Needers.paginate(queryObj, pagObj).then(response => {
    res.send(response)
  }).catch((e) => {
    console.log(e)
  })
})

module.exports = router
