import axios from 'axios'
import store from '../store'
import router from '../router'

export default() => {
  let http = axios.create({
    baseURL: process.env.API_BASE
  })
  // Set auth header if exists in local storage
  http.defaults.headers.common['x-auth'] = localStorage.getItem('authorization_token') ? localStorage.getItem('authorization_token') : null
  http.defaults.headers.common['x-auth-front'] = localStorage.getItem('authorization_token_front') ? localStorage.getItem('authorization_token_front') : null
  // catch 401 requests
  http.interceptors.response.use(
    response => response,
    error => {
      const {status} = error.response
      if (status === 500) {
        store.dispatch('common/setMessage', 'Doslo je do greske na serveru!')
      } else if (status === 401) {
        store.dispatch('auth/logoutLocal')
        router.push('/')
      } else if (status === 403) {
        store.dispatch('common/setMessage', `Akcija zabranjena: ${error.response.request.responseURL}`)
      } else if (status === 400) {
        let dataObj = error.response.data.data
        const errorsFromServer = {}
        let showErrors = false
        if (dataObj.stat === 'FAILED' && dataObj.banners && dataObj.banners.length) {
          dataObj.banners.forEach(banner => {
            if (banner.objectState === 'INVALID') {
              let errors = Object.keys(banner.violatedConstraints)
              errorsFromServer[banner.title] = errors
              showErrors = true
            }
          })
        } else if (dataObj.stat === 'FAILED' && dataObj.sliders && dataObj.sliders.length) {
          dataObj.sliders.forEach(slider => {
            if (slider.objectState === 'INVALID') {
              let errors = Object.keys(slider.violatedConstraints)
              errorsFromServer[slider.name] = errors
              showErrors = true
            }
          })
        }

        // show errors
        if (showErrors) {
          store.dispatch('bannerAdmin/setErrors', errorsFromServer)
          store.dispatch('bannerAdmin/errorDialog')
        }
      }
      return Promise.reject(error.response)
    }
  )

  return http
}
