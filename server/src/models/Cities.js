const mongoose = require('mongoose')
const _ = require('lodash')
const mongoosePaginate = require('mongoose-paginate')

const CitiesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  }
})

CitiesSchema.plugin(mongoosePaginate)

CitiesSchema.methods.toJSON = function () {
  let cities = this
  let cityObject = cities.toObject()

  return _.pick(cityObject, ['name'])
}

const Cities = mongoose.model('Cities', CitiesSchema)

module.exports = {Cities}
