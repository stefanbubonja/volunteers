const mongoose = require('mongoose')

const RoleSchema = new mongoose.Schema({
  role_id: {
    type: Number,
    require: true
  },
  role_name: {
    type: String,
    require: true
  },
  name: {
    type: String,
    require: true
  },
  path: {
    type: String,
    require: false
  },
  marketCoverage: {
    type: Array,
    require: false
  }
})

const Role = mongoose.model('Role', RoleSchema)

module.exports = {Role}
