export default {
  $vuetify: {
    dataIterator: {
      rowsPerPageText: 'Stavki po stranici:',
      rowsPerPageAll: 'Sve',
      pageText: '{0}-{1} od {2}',
      noResultsText: 'Ništa pronadjeno',
      nextPage: 'Sledeća strana',
      prevPage: 'Prethodna strana'
    },
    dataTable: {
      rowsPerPageText: 'Stavki po stranici:'
    },
    noDataText: 'Nema podataka'
  },
  user: {
    email: 'Email',
    name: 'Ime',
    role: 'Uloga',
    actions: 'Akcije',
    loginError: 'Pogrešan email ili password, pokušajte ponovo!'
  },
  menu: {
    home: 'Početna',
    competitions: 'Top lige',
    spec: 'Speciali',
    quotas: 'Upload kvota',
    icons: 'Upload ikonica',
    bettingPlaces: 'Uplatna mesta',
    cities: 'Gradovi',
    banners: 'Upravljanje banerima',
    news: 'Upravljanje vestima',
    agents: 'Agenti',
    volonters: 'Volonteri'
  },
  other: {
    users: 'Korisnici',
    search: 'Pretraži',
    logout: 'Odjava',
    save: 'Sačuvaj',
    config: 'Konfig',
    cities: 'Gradovi'
  },
  breadCrumbs: {
    managing_news: 'Upravljanje vestima',
    news_news: 'Nova vest',
    news_edit: 'Ažuriranje vesti'
  },
  FIELD_REQUIRED: 'Polje {field} je obavezno',
  FIELD_NUMERIC: 'Polje {field} moze sadrzati samo brojeve',
  FIELD_IMAGE: 'Podržani fajlovi su .jpg, .jpeg i .png',
  agents: {
    agentId: 'ID agenta',
    name: 'Ime',
    address: 'Adresa',
    phone: 'Telefon',
    city: 'Grad/Opstina',
    availability: 'Dostupnost',
    language: 'Jezik',
    custom1: 'Custom 1',
    custom2: 'Custom 2',
    actions: 'Akcije'
  }
}
