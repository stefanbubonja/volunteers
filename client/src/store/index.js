import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import auth from './modules/auth'
import user from './modules/user'
import common from './modules/common'
import roles from './modules/roles'
import volonters from './modules/volonters'
import front from './modules/front'

Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    common,
    auth,
    user,
    roles,
    volonters,
    front
  },
  strict: debug,
  plugins: [createLogger({
    collapsed: true
  })]
})
