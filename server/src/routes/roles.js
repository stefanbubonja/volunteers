const { apiBannerAdmin } = require('../../config/config')
const express = require('express')
const router = express.Router()
const axios = require('axios')

const _ = require('lodash')
const {Role} = require('../models/Role')
const {authenticate} = require('../middleware/authenticate')
const permit = require('../middleware/permission')

// GET /roles
router.get('/', authenticate, permit('SUPERADMIN', 'BANNERADMIN'), (req, res) => {
  Role.find({}, (err, roles) => {
    if (err) {
      res.status(400).send(err)
    } else {
      res.status(200).send(roles)
    }
  })
})

// POST /roles
router.post('/', authenticate, permit('SUPERADMIN'), (req, res) => {
  let body = _.pick(req.body, ['name', 'role_name', 'role_id', 'path', 'marketCoverage'])
  let role = new Role(body)

  role.save().then((role) => {
    let roleForBannerAdmin = {
      role_name: body.role_name,
      role_id: body.role_id,
      marketCoverage: body.marketCoverage
    }
    axios.post(`${apiBannerAdmin}/role`, {role: roleForBannerAdmin}, {
      headers: {
        Authorization: 'Bearer ' + req.headers['admin-banner-token']
      }}).then(response => {
      console.log('Korisnik banner admin response:', response.data)
      if (response.data.stat === 'SUCCESS') {
        console.log('Korisnik uspesno dodat na baner admin', response.data)
        res.status(200).send(role)
      }
    }).catch((e) => {
      console.log('GRESKA dodavanje korisnika na baner admin', e)
      res.status(400).send(roleForBannerAdmin)
    })
  }).catch((e) => {
    res.status(400).send(e)
  })
})

// DELETE /roles/:id
router.delete('/:id', authenticate, permit('SUPERADMIN'), (req, res) => {
  Role.findByIdAndRemove(req.params.id, (err, doc) => {
    if (doc !== null) {
      res.status(200).send(doc)
    } else {
      res.status(400).send(err)
    }
  })
})

module.exports = router
