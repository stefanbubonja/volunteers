// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

import Vuetify from 'vuetify'
import VeeValidate from 'vee-validate'
import VueI18n from 'vue-i18n'
import messages from './locale/messages'

import moment from 'moment'

Vue.config.productionTip = true

Vue.use(VeeValidate)
Vue.use(VueI18n)
const i18n = new VueI18n({
  locale: localStorage.getItem('lang') ? localStorage.getItem('lang') : 'sr', // set locale
  fallbackLocale: 'en',
  messages // set locale messages
})
Vue.use(Vuetify, {
  lang: {
    t: (key, ...params) => i18n.t(key, params)
  }
})

// Filters
Vue.filter('size', function (bytes) {
  let sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
  if (bytes === 0) return '0 Byte'
  let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)))
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i]
})

Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(String(value)).format('MM/DD/YYYY')
  }
})

Vue.filter('formatDateObj', function (date, format = 'MM/DD/YYYY') {
  if (date) {
    return moment(date).format(format)
  }
})

Vue.filter('banerDate', function (value, format) {
  if (value) {
    let dateTimeSplited = value.split(' ')
    let timeSplited = dateTimeSplited[1].split(':')
    let dateSplited = dateTimeSplited[0].split('-')
    let mydate = new Date(dateSplited[2], dateSplited[1] - 1, dateSplited[0])
    mydate.setHours(timeSplited[0], timeSplited[1], timeSplited[2])
    return moment(mydate).format(format)
  }
})

Vue.filter('convertDate', function (value) {
  if (typeof value === 'string' && value !== '') {
    let dateTimeSplited = value.split(' ')
    let timeSplited = dateTimeSplited[1].split(':')
    let dateSplited = dateTimeSplited[0].split('-')
    let mydate = new Date(dateSplited[2], dateSplited[1] - 1, dateSplited[0])
    mydate.setHours(timeSplited[0], timeSplited[1], timeSplited[2])
    return mydate
  }
  return value
})

const dictionary = {
  en: {
    messages: {
      required: () => 'Ovo polje je obavezno',
      numeric: () => 'Ovo polje mora biti broj',
      email: () => 'Polje Email nije validno',
      min: () => 'Polje nije dovoljne dužine'
    }
  }
}

// Override and merge the dictionaries
VeeValidate.Validator.localize(dictionary)

/* eslint-disable no-new */
new Vue({
  i18n,
  el: '#app',
  store,
  router,
  render: h => h(App)
})
