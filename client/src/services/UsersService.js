import Api from '@/services/Api'

export default {
  getUsers (pagination) {
    return Api().get('/users', {params: {
      page: pagination.page,
      perPage: pagination.rowsPerPage,
      sortby: pagination.sortBy,
      order: pagination.descending ? 1 : -1,
      search: pagination.search
    }})
  },
  addUser (user) {
    return Api().post('/users', user)
  },
  updateUser (user) {
    return Api().put('/users', user)
  },
  deleteUser (id) {
    return Api().delete(`/users/${id}`)
  }
}
