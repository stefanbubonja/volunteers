const { apiBannerAdmin } = require('../../config/config')
const express = require('express')
const router = express.Router()
const _ = require('lodash')
const axios = require('axios')

const {User} = require('../models/User')
const {authenticate} = require('../middleware/authenticate')
const permit = require('../middleware/permission')

// POST /users
router.post('/', authenticate, permit('SUPERADMIN', 'BANNERADMIN'), (req, res) => {
  var body = _.pick(req.body, ['email', 'password', 'name', 'role'])
  var user = new User(body)

  user.save().then(() => {
    return user.generateAuthToken()
  }).then((token) => {
    res.header('x-auth', token).send(user)
  }).catch((e) => {
    res.status(400).send(e)
  })
})

// PUT /users
router.put('/', authenticate, permit('SUPERADMIN'), (req, res) => {
  var body = _.pick(req.body, ['email', 'password', 'name', 'role'])

  User.findById(req.body._id, function (err, user) {
    if (user) {
      user.email = body.email
      user.name = body.name
      user.role = body.role
      if (_.has(body, 'password') && !_.isEmpty(body.password)) { user.password = body.password }
      user.save().then(() => {
        res.status(200).send(user)
      }).catch((err) => {
        res.status(400).send(err)
      })
    } else {
      res.status(400).send(err)
    }
  })
})

// GET /users
router.get('/', authenticate, permit('SUPERADMIN', 'BANNERADMIN'), (req, res) => {
  var pagObj = {
    page: parseInt(req.query.page),
    limit: parseInt(req.query.perPage) ? parseInt(req.query.perPage) : 10
  }
  var queryObj = {}

  if (req.query.sortby) {
    var sort = {}
    sort[req.query.sortby] = req.query.order
    pagObj.sort = sort
  }

  if (req.query.search && req.query.search !== '') {
    queryObj = {email: { $regex: '.*' + req.query.search + '.*' }}
  }
  User.paginate(queryObj, pagObj).then(response => {
    res.send(response)
  }).catch((e) => {
    console.log(e)
  })
})

// GET /users/me
router.get('/me', authenticate, (req, res) => {
  res.send(req.user)
})

// POST /users/login {email, password}
router.post('/login', (req, res) => {
  var body = _.pick(req.body, ['email', 'password'])

  User.findByCredentials(body.email, body.password).then((user) => {
    return user.generateAuthToken().then(token => {
      res.setHeader('x-auth', token)
      res.send(user)
    })
  }).catch((e) => {
    res.send({error: true, status: 'WRONG_CREDENTIALS'})
  })
})

// DELETE /users/me/token
router.delete('/me/token', authenticate, (req, res) => {
  req.user.removeToken(req.token).then(() => {
    res.status(200).send()
  }, () => {
    res.status(400).send()
  })
})

// DELETE /users/:id
router.delete('/:id', authenticate, permit('SUPERADMIN'), (req, res) => {
  User.findByIdAndRemove(req.params.id, (err, doc) => {
    if (doc !== null) {
      res.status(200).send(doc)
    } else {
      res.status(400).send(err)
    }
  })
})

module.exports = router
