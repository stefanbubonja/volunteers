const mongoose = require('mongoose')
const _ = require('lodash')
const mongoosePaginate = require('mongoose-paginate')

const NeedersSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  age: {
    type: Number,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  helpType: {
    type: Array,
    required: true
  },
  helpText: {
    type: String,
    required: false
  },
  status: {
    type: String,
    required: true
  },
  volonter: {
    type: Object,
    required: false
  },
  created: {
    type: Date,
    required: true
  },
  ip: {
    type: String,
    required: true
  }
})

NeedersSchema.plugin(mongoosePaginate)

NeedersSchema.methods.toJSON = function () {
  let needers = this
  let neederObject = needers.toObject()

  return _.pick(neederObject, ['_id', 'name', 'surname', 'address', 'phone', 'city', 'age', 'helpType', 'helpText', 'status', 'ip', 'created', 'volonter'])
}

const Needers = mongoose.model('Needers', NeedersSchema)

module.exports = {Needers}
