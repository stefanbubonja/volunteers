const mongoose = require('mongoose')
const _ = require('lodash')
const mongoosePaginate = require('mongoose-paginate')

const ContentSchema = new mongoose.Schema({
  phones: {
    type: Array,
    required: true
  },
  custom: {
    type: String,
    required: false
  }
})

ContentSchema.plugin(mongoosePaginate)

ContentSchema.methods.toJSON = function () {
  let cont = this
  let contObj = cont.toObject()

  return _.pick(contObj, ['phones', 'custom'])
}

const Content = mongoose.model('Content', ContentSchema)

module.exports = {Content}
