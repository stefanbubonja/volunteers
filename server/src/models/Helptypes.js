const mongoose = require('mongoose')
const _ = require('lodash')
const mongoosePaginate = require('mongoose-paginate')

const HelpTypesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  id: {
    type: Number,
    required: true
  }
})

HelpTypesSchema.plugin(mongoosePaginate)

HelpTypesSchema.methods.toJSON = function () {
  let helps = this
  let helpObj = helps.toObject()

  return _.pick(helpObj, ['name', 'id'])
}

const Helps = mongoose.model('Helps', HelpTypesSchema)

module.exports = {Helps}
