import VolontersService from '../../services/VolontersService'

const state = {
  volonters: [],
  pagination: {
    page: 1,
    sortBy: 'name',
    descending: false
  }
}

const getters = {
  volonters: state => state.volonters,
  pagination: state => state.pagination,
  getVolonterById: state => id => state.volonters.find(Volonter => Volonter._id === id)
}

const actions = {
  getVolonters ({commit, dispatch}) {
    dispatch('common/setLoading', true, {root: true})
    return VolontersService.getVolonters(state.pagination).then(res => {
      console.log(res, 'resresresres')
      commit('setVolonters', res.data.docs)
      commit('setPagination', {
        page: parseInt(res.data.page),
        rowsPerPage: res.data.limit,
        totalItems: res.data.total
      })
      dispatch('common/setLoading', false, {root: true})
      return Promise.resolve()
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  },
  updatePagination ({commit, dispatch}, pagination) {
    commit('setPagination', pagination)
    dispatch('getVolonters')
  },
  addVolonter ({commit, dispatch}, data) {
    dispatch('common/setLoading', true, {root: true})
    return VolontersService.addVolonter(data).then(res => {
      dispatch('common/setLoading', false, {root: true})
      commit('addVolonter', res.data)
      return Promise.resolve(res.data)
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  },
  updateVolonter ({commit, dispatch}, data) {
    dispatch('common/setLoading', true, {root: true})
    return VolontersService.updateVolonter(data).then(res => {
      dispatch('common/setLoading', false, {root: true})
      commit('updateVolonter', res.data)
      return Promise.resolve(res.data)
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  },
  deleteVolonter ({commit, dispatch}, id) {
    dispatch('common/setLoading', true, {root: true})
    return VolontersService.deleteVolonter(id).then(res => {
      dispatch('common/setLoading', false, {root: true})
      commit('deleteVolonter', id)
      return Promise.resolve()
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  }
}

const mutations = {
  setVolonters (state, volonters) {
    state.volonters = volonters
  },
  addVolonter (state, Volonter) {
    const tmpVolonters = state.volonters.slice()
    tmpVolonters.push(Volonter)
    state.volonters = tmpVolonters
  },
  updateVolonter (state, Volonter) {
    const tmpVolonters = state.volonters.slice()
    let index = tmpVolonters.findIndex(el => el._id === Volonter._id)
    tmpVolonters.splice(index, 1, Volonter)
    state.volonters = tmpVolonters
  },
  deleteVolonter (state, id) {
    const tmpVolonters = state.volonters.slice()
    tmpVolonters.splice(tmpVolonters.findIndex(el => el._id === id), 1)
    state.volonters = tmpVolonters
  },
  setPagination (state, pagination) {
    state.pagination = {...state.pagination, ...pagination}
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
