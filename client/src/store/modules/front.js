import contentService from '../../services/ContentService'

const state = {
  phone: [],
  modal: {
    show: false,
    message: '',
    image: '',
    redirect: '/'
  }
}

const getters = {
  getPhoneNumber: (state) => {
    return state.phone
  },
  modal: (state) => {
    return state.modal
  }
}

const actions = {
  getPhone ({ commit }) {
    contentService.getContent().then(res => {
      if (res.data) {
        commit('setPhone', res.data[0], res.data[0].phones.join(', '))
      }
    })
  },
  setModal ({commit}, modal) {
    commit('setModalData', modal)
  }
}

const mutations = {
  setPhone (state, phone) {
    state.phone = phone
  },
  setModalData (state, modal) {
    state.modal = modal
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
