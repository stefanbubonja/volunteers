var {Volonters} = require('../models/Volonters')

const frontauth = async (req, res, next) => {
  let token = req.header('x-auth-front')

  Volonters.findByToken(token).then((volonter) => {
    if (!volonter) {
      // eslint-disable-next-line
      return Promise.reject()
    }
    req.volonter = volonter
    req.tokenfront = token
    next()
  }).catch((err) => {
    res.status(401).send(err)
  })
}

module.exports = {frontauth}
