const mongoose = require('mongoose')
const _ = require('lodash')
const validator = require('validator')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const mongoosePaginate = require('mongoose-paginate')
const { jwtSecret, jwtExpire } = require('../../config/config')

const VolontersSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  availability: {
    type: Boolean,
    required: true
  },
  helpType: {
    type: Array,
    required: true
  },
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email'
    }
  },
  password: {
    type: String,
    required: true,
    minlength: 6
  },
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }]
})

VolontersSchema.plugin(mongoosePaginate)

VolontersSchema.methods.toJSON = function () {
  let volonters = this
  let volonterObject = volonters.toObject()

  return _.pick(volonterObject, [
    '_id',
    'email',
    'name',
    'surname',
    'phone',
    'city',
    'availability',
    'helpType'])
}

VolontersSchema.methods.generateAuthToken = function () {
  let user = this
  let access = 'auth'
  let token = jwt.sign({_id: user._id.toHexString(), access}, jwtSecret, {
    expiresIn: jwtExpire
  }).toString()

  user.tokens = user.tokens.concat([{access, token}])
  return user.save().then(() => {
    return token
  })
}

VolontersSchema.statics.findByToken = function (token) {
  let User = this
  let decoded

  try {
    decoded = jwt.verify(token, jwtSecret)
  } catch (e) {
    return Promise.reject(e)
  }

  return User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  })
}

VolontersSchema.statics.findByCredentials = function (email, password) {
  let Volonter = this
  return Volonter.findOne({email}).then((user) => {
    if (!user) {
      return Promise.reject
    }

    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.password, (err, res) => {
        if (res) {
          resolve(user)
        } else {
          reject(err)
        }
      })
    })
  })
}

VolontersSchema.methods.removeToken = function (token) {
  let volonter = this
  return volonter.update({
    $pull: {
      tokens: {token}
    }
  })
}

VolontersSchema.pre('save', function (next) {
  let user = this

  if (user.isModified('password')) {
    bcrypt.genSalt(10, (e, salt) => {
      bcrypt.hash(user.password, salt, (e, hash) => {
        user.password = hash
        next()
      })
    })
  } else {
    next()
  }
})

const Volonters = mongoose.model('Volonters', VolontersSchema)

module.exports = {Volonters}
