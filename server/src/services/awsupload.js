const { accessKey, awsSecret, awsRegion } = require('../../config/config')
const AWS = require('aws-sdk')

AWS.config.update({
    accessKeyId: accessKey,
    secretAccessKey: awsSecret,
    region: awsRegion
})

const uploadImageToAWS = (file, bucket) => {
    return new Promise((resolve, reject) => {
        const s3 = new AWS.S3()
        let params = {
            Bucket: bucket,
            Key: file.originalname,
            Body: file.buffer,
            ContentType: file.mimetype,
            ACL: 'public-read'
        }
        s3.upload(params, function (err, data) {
            if (err) reject(err)
            resolve(data)
        })
    })
}

exports.uploadImageToAWS = uploadImageToAWS