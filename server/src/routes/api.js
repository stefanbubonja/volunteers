const express = require('express')
const router = express.Router()
const _ = require('lodash')

const {Cities} = require('../models/Cities')
const {Content} = require('../models/Content')
const {Helps} = require('../models/Helptypes')
const cities = []

router.get('/get-cities', async (req, res) => {
  Cities.find().then(response => {
    res.send(response)
  }).catch((e) => {
    res.status(400).send(e)
  })
})

router.get('/get-content', async (req, res) => {
  Content.find().then(response => {
    res.send(response)
  }).catch((e) => {
    res.status(400).send(e)
  })
})

router.get('/get-helps', async (req, res) => {
  Helps.find().then(response => {
    res.send(response)
  }).catch((e) => {
    res.status(400).send(e)
  })
})

// router.get('/import-cities', async (req, res) => {
//   for (city of cities) {
//     let cityObj = new Cities(city)
//     let citySaved = await cityObj.save()
//   }
// })

// router.post('/insert-content', async (req, res) => {
//   try {
//     let contObj = new Content(req.body)
//     let cont = await contObj.save()
//     res.send(cont)
//   } catch (e) {
//     res.status(400).send(e)
//   }
// })

// router.post('/insert-helptype', async (req, res) => {
//   try {
//     let helpsObj = new Helps(req.body)
//     let helps = await helpsObj.save()
//     res.send(helps)
//   } catch (e) {
//     res.status(400).send(e)
//   }
// })

module.exports = router