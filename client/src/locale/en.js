export default {
  $vuetify: {
    dataIterator: {
      rowsPerPageText: 'Items per page:',
      rowsPerPageAll: 'All',
      pageText: '{0}-{1} of {2}',
      noResultsText: 'No matching records found',
      nextPage: 'Next page',
      prevPage: 'Previous page'
    },
    dataTable: {
      rowsPerPageText: 'Rows per page:'
    },
    noDataText: 'No data available'
  },
  user: {
    email: 'Email',
    name: 'Name',
    role: 'Role',
    actions: 'Actions',
    loginError: 'Wrong email or password, please try again.'
  },
  menu: {
    home: 'Home',
    competitions: 'Top competitions',
    spec: 'Specials',
    quotas: 'Quotas upload',
    banners: 'Managing banners'
  },
  other: {
    users: 'Users',
    search: 'Search',
    logout: 'Logout',
    save: 'Save',
    config: 'Config'
  },
  breadCrumbs: {
    managing_banners: 'Managing banners',
    slider_creation: 'Slider creation',
    target_groups: 'Target groups',
    banners_db: 'Banner base',
    banners_create: 'Adding banner',
    banners_edit: 'Updating banner',
    banners_assign: 'Updating slider'
  },
  FIELD_REQUIRED: 'Field {field} is required',
  FIELD_NUMERIC: 'Filed {field} can contain numbers only',
  FIELD_IMAGE: 'Supported files are .jpg, .jpeg and .png'
}
