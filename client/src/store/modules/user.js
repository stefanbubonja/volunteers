import user from '../../services/UsersService'

const state = {
  users: [],
  pagination: {
    page: 1,
    sortBy: 'email',
    descending: false
  }
}

const getters = {
  users (state) {
    return state.users
  },
  pagination (state) {
    return state.pagination
  },
  getUserById: (state) => (id) => {
    return state.users.find(user => user._id === id)
  }
}

const actions = {
  getUsers ({commit, dispatch}) {
    dispatch('common/setLoading', true, {root: true})
    return user.getUsers(state.pagination).then(res => {
      commit('setUsers', res.data.docs)
      commit('setPagination', {
        page: parseInt(res.data.page),
        rowsPerPage: res.data.limit,
        totalItems: res.data.total
      })
      dispatch('common/setLoading', false, {root: true})
      return Promise.resolve()
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  },
  updatePagination ({commit, dispatch}, pagination) {
    commit('setPagination', pagination)
    dispatch('getUsers')
  },
  addUser ({commit, dispatch}, userData) {
    dispatch('common/setLoading', true, {root: true})
    const role = {
      role_id: userData.role.role_id,
      role_name: userData.role.role_name,
      path: userData.role.path ? userData.role.path : null,
      marketCoverage: userData.role.marketCoverage ? userData.role.marketCoverage : null
    }
    userData.role = role
    return user.addUser(userData).then(res => {
      dispatch('common/setLoading', false, {root: true})
      commit('addUser', res.data)
      return Promise.resolve(res.data)
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  },
  updateUser ({commit, dispatch}, userData) {
    dispatch('common/setLoading', true, {root: true})
    const role = {
      role_id: userData.role.role_id,
      role_name: userData.role.role_name,
      path: userData.role.path ? userData.role.path : null,
      marketCoverage: userData.role.marketCoverage ? userData.role.marketCoverage : null
    }
    userData.role = role
    return user.updateUser(userData).then(res => {
      dispatch('common/setLoading', false, {root: true})
      commit('updateUser', res.data)
      return Promise.resolve(res.data)
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  },
  deleteUser ({commit, dispatch}, id) {
    dispatch('common/setLoading', true, {root: true})
    return user.deleteUser(id).then(res => {
      dispatch('common/setLoading', false, {root: true})
      commit('deleteUser', id)
      return Promise.resolve()
    }).catch(err => {
      dispatch('common/setLoading', false, {root: true})
      return Promise.reject(err)
    })
  }
}

const mutations = {
  setUsers (state, users) {
    state.users = users
  },
  addUser (state, user) {
    const tmpUsers = state.users.slice()
    tmpUsers.push(user)
    state.users = tmpUsers
  },
  updateUser (state, user) {
    let tmpUsers = state.users.slice()
    let index = tmpUsers.findIndex(el => el._id === user._id)
    tmpUsers.splice(index, 1, user)
    state.users = tmpUsers
  },
  deleteUser (state, id) {
    const tmpUsers = state.users.slice()
    tmpUsers.splice(tmpUsers.findIndex(el => el._id === id), 1)
    state.users = tmpUsers
  },
  setPagination (state, pagination) {
    state.pagination = {...state.pagination, ...pagination}
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
